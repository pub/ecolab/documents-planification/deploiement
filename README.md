# docs-planification-deployment


## Déploiement

L’application est actuellement déployée sur le SSP-Cloud, mais elles sera peut être déployée sur une instance du ministère dans le futur.

### Dockerisation

Actuellement, on utilise un *gitlab runner* pour dockeriser les différentes instances du projet : l’import des données, l’application et le projet dbt. Chaque projet doit donc contenir :

1. Un fichier **.gitlab-ci.yml** : le but de fichier est de dire à gitlab de lancer le docker à chaque fois que la branche main est mis à jour (on peut aussi ajouter des test ou d’autres contraintes mais ce n’est pas le cas pour ce projet actuellement). Le fichier est de cette forme (on peut utiliser le même fichier pour tous les projets, il n’y rien à modifier) :

```yaml
docker:
  stage: deploy
  cache: []
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - export DBT_PROFILES_DIR=$CI_PROJECT_DIR/dbt_proj
    - /kaniko/executor
      --context $CI_PROJECT_DIR
      --dockerfile $CI_PROJECT_DIR/Dockerfile
      --destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}
      --destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_BRANCH}
  only:
    - main
    - tags
```

1. Un Dockerfile, il faut mettre dedans toutes les instructions permettant de lancer l’application, voici par exemple le dockerfile du projet dbt (le plus compliqué) :

```docker
# Création de l'environnement python, Obligatoire pour tous les projets
FROM python:3.10

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

# Copie des fichiers importants et du répertoir principal
COPY ./requirements-tests.txt requirements-tests.txt
COPY ./requirements.txt requirements.txt
COPY ./setup.py setup.py
COPY ./dbt_proj dbt_proj

# Installation des requirements
RUN pip install -r requirements-tests.txt

# Je n'ai pas tout compris mais cette partie est nécessaire
RUN pip install --upgrade pip setuptools wheel \
    && pip install -e . \
    && pip cache purge \
    && rm -rf /root/.cache/pip

# Spécificité du projet dbt : il faut se placer dans le répertoire du projet dbt pour pouvoir le lancer
WORKDIR dbt_proj

# Commande à lancer avant la commande principale du déploiement
RUN dbt deps --profiles-dir . --target prod

# L'entrypoint est ce qui est écrit avant CMD
ENTRYPOINT ["dbt"]

# La commande (CMD) sera éxécutée uniquement lorsque l'on déploiera le projet avec kubernetes : le gitlab runner n'éxécute pas la commande
CMD ["run", "-m", "+marts", "--profiles-dir", ".", "--target", "prod"]
```

### Déploiement sur le SSP-Cloud avec Kubernetes : (https://gitlab-forge.din.developpement-durable.gouv.fr/pub/ecolab/documents-planification/deploiement)

Pour déployer le projet, vous devez d’abord lancer une instance de VS-Code sur le SSP-Cloud en étant admin Kubernetes (On peut le sélectionner lorsque l’on crée le service).

**Déploiement de la webapp :**

On se base sur ce template https://github.com/InseeFrLab/sspcloud-tutorials/blob/main/deployment/shiny-app.md qui est à l’origine pour une app R-shiny.

Il faut d’abord créer un fichier Chart.yaml et values.yaml (il est conseillé de mettre ces fichiers sur git pour ne pas avoir à les réécrire lors de mises à jour.

Le fichier Chart.yaml est excatement celui-ci :

```yaml
apiVersion: v2
name: shiny-app-template
version: 1.1.0
dependencies:
  - name: shiny
    version: 0.25.1
    repository: https://inseefrlab.github.io/helm-charts
```

Le fichier values.yaml doit suivre ce template :

```yaml
shiny:
  image:
		# Lien vers le registry, pour le trouver, il faut aller sur le projet gitlab, cliquer sur packages and registries puis sur container registry, et enfin sur le petit symbole 'copier' dans la page qui s'affiche
    repository: registry.gitlab-forge.din.developpement-durable.gouv.fr/pub/ecolab/documents-planification/app
    tag: main
    pullPolicy: Always
  imagePullSecrets: # secret de l'image du docker (voir après pour comment le créer)
  - name: docs-plan-regcred
  networking:
    service:
      port: 8501 # port de streamlit
  ingress:
    enabled: true
    hostname: docs-plan.lab.sspcloud.fr  # lien vers l'app, il doit fini en .lab.sspcloud.fr mais on peut mettre ce qu'on veut avant
  env:  # toutes les variables d'environnement utilisées par l'app, voir ci-après pour savoir comment créer le secret correspondant
  - name: POSTGRESQL_PASSWORD
    valueFrom:
      secretKeyRef:
        name: user-bdd-postgre
        key: password
  - name: POSTGRESQL_HOST
    valueFrom:
      secretKeyRef:
        name: user-bdd-postgre
        key: host
  - name: POSTGRESQL_DB
    valueFrom:
      secretKeyRef:
        name: user-bdd-postgre
        key: database
  - name: POSTGRESQL_USER
    valueFrom:
      secretKeyRef:
        name: user-bdd-postgre
        key: user
  - name: POSTGRESQL_PORT
    valueFrom:
      secretKeyRef:
        name: user-bdd-postgre
        key: port
  - name: POSTGRE_SCHEMA
    valueFrom:
      secretKeyRef:
        name: user-bdd-postgre
        key: schema
```

Ensuite, il faut créer les secrets correspondants :

- Le secret docs-plan-regcred doit être créé en entrant cette commande dans le terminal :
    
    ```yaml
    kubectl create secret docker-registry docs-plan-regcred
    --docker-server=<your-registry-server>
    --docker-username=<your-name> 
    --docker-password=<your-pword> 
    --docker-email=<your-email>
    ```
    
    <your-registry-server> est le lien vers le registry gitlab : pour le trouver, il faut aller sur le projet gitlab, cliquer sur packages and registries puis sur container registry, et enfin sur le petit symbole 'copier' dans la page qui s'affiche
    
    <your-name> est le nom du token du projet et <your-pword> est le token du projet
    
    Pour créer le token, il faut aller sur la page gitlab du projet puis aller dans settings puis access tokens. Il faut ensuite créer un token en ayant coché la case *read_registry.* Remarque : il faut être au moins maintainer du projet pour pouvoir créer le token.
    
    <your-email> est votre e-mail
    
- Le secret avec les variables d’environnement (user-bdd-postgre) doit être créé à l’aide d’un fichier *quelconque.yaml* qui prend la forme suivante :
    
    ```yaml
    apiVersion: v1
    kind: Secret
    metadata:
      name: user-bdd-postgre
    type: Opaque
    stringData:
      password: 
      host: postgresql-158554
      database: docs-plan
      user: app
      port: 5432
      schema: dbt_marts
    ```
    
    En remplissant le mot de passe du user app à côté de password.
    
    Pour que le secret prenne effet, il faut exécuter la commande suivante : 
    
    ```bash
    kubectl apply -f quelconque.yaml
    ```
    

Ces secrets restent en application pour toujours, même si votre service vscode expire.

Une fois que tout cela est fait, il faut aller dans le répertoire où se trouve les fichiers Chart.yaml et values.yaml et exécuter les commandes suivantes :

```bash
helm dependency update ./
helm install ./ --generate-name
```

Si tout a fonctionné, un message devrait confirmer l'instanciation du chart. On peut vérifier que ce dernier a bien été déployé avec la commande `helm ls`, qui liste l'ensemble des instances de Chart helm déployées sur le cluster.

****************************************************Mise à jour de la webapp :****************************************************

Après avoir push une nouvelle version de votre app sur la branche main du git de l’application. Vous pouvez mettre à jour votre application avec les commandes suivantes :

```bash
helm upgrade nom_chart_deploye ./
kubectl rollout restart deployment nom_chart_deploye-shiny
```

Vous pouvez connaître le nom du chart déployé avec  `helm ls` . Attention, il faut rajouter *******-shiny******* dans le nom du chart lors de la commande *kubectl rollout restart.*

****************************************************************************Déploiement de l’import des données ou de dbt :****************************************************************************

Il faut cette fois-ci créer un fichier cronjob.yaml de cette forme :

```yaml
apiVersion: batch/v1
kind: CronJob
metadata:
  name: cronjob-dbt
spec:
  schedule: "0 0 * * *" # Le cronjob se lance une fois par jour
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: container-dbt
            image: registry.gitlab-forge.din.developpement-durable.gouv.fr/pub/ecolab/documents-planification/dbt:main # registry du docker
            imagePullPolicy: Always
            env: # variables d'environnement
            - name: POSTGRESQL_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: user-dbt-bdd-postgre
                  key: password
            - name: POSTGRESQL_HOST
              valueFrom:
                secretKeyRef:
                  name: user-dbt-bdd-postgre
                  key: host
            - name: POSTGRESQL_DB
              valueFrom:
                secretKeyRef:
                  name: user-dbt-bdd-postgre
                  key: database
            - name: POSTGRESQL_USER
              valueFrom:
                secretKeyRef:
                  name: user-dbt-bdd-postgre
                  key: user
            - name: POSTGRESQL_PORT
              valueFrom:
                secretKeyRef:
                  name: user-dbt-bdd-postgre
                  key: port
            - name: POSTGRE_SCHEMA
              valueFrom:
                secretKeyRef:
                  name: user-dbt-bdd-postgre
                  key: schema
          imagePullSecrets: #secret du regitry
          - name: dbt-docs-plan-regcred
          restartPolicy: OnFailure
```

Les secrets se créent de la même façon que pour le déploiement de l’app

Une fois le fichier cronjob.yaml et les secrets créés, il suffit de lancer la commande suivante pour lancer le cronjob :

```bash
kubectl apply -f chemin_vers_le_cronjob/cronjob.yaml
```

Le cronjob se met automatiquement à jour lorsque l’on push une nouvelle version de l’import ou de dbt dans main.

Si vous  voulez lancer le cronjob à un autre moment que son heure programmée, vous pouvez éxécuter cette commande :

```bash
kubectl create job --from=cronjob/<NOM_DU_CRONJOB> <NOM_DU_JOB_MANUEL>
```

### Commandes utiles de Kubernetes

Il y a plusieurs commandes très utile dans kubernetes pour debugger l’application

`helm ls` donne la liste des chart helm déployés

`kubectl get deployments` fait sensiblement la même chose que helm ls

`kubectl get cronjobs`  donne la liste des cronjob

`kubectl get jobs`  donne la liste des jobs

`kubectl get pods`  donne la liste des pods, un pods est la plus petite entité de kubernetes, c’est ce qui est vraiment exécuté par l’application

`kubectl describe type_objet nom_de_l_objet` donne des informations sur l’objet concernés

`kubectl logs nom_du_pod` permet d’afficher la même chose que ce qu’on aurait vu dans le terminal en lançant le code en local